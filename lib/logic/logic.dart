import 'package:flutter/material.dart';
import 'package:tic_tac_toe/board/board_state.dart';
import 'package:tic_tac_toe/players/player.dart';

class GameLogic {
  static final List<List<int>> _gameOverCondition = [
    // horizontal conditions
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    // vertical conditions
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    // cross conditions
    [0, 4, 8],
    [2, 4, 6],
  ];

  static bool _hasWin(PLAYER player, List<PLAYER> boardState) {
    for (int j = 0; j < _gameOverCondition.length; j++) {
      var winArr = _gameOverCondition[j];

      bool win = true;

      winArr.forEach((i) {
        if (boardState.elementAt(i) != player) win = false;
      });

      if (win == true) return true;
    }

    return false;
  }

  // public helper functions
  static PLAYER winner(List<PLAYER> currentBoardState) {
    if (_hasWin(PLAYER.CIRCLE, currentBoardState))
      return PLAYER.CIRCLE;
    else if (_hasWin(PLAYER.CROSS, currentBoardState))
      return PLAYER.CROSS;
    else
      return PLAYER.EMPTY;
  }
}
