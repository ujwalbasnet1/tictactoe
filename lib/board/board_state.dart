import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tic_tac_toe/logic/logic.dart';
import 'package:tic_tac_toe/players/player.dart';
import 'package:tic_tac_toe/utils.dart';

class BoardState {
  List<PLAYER> _boardState;
  final PLAYER player;

  PLAYER _enemy;

  bool _playerTurn = true;

  static const List<PLAYER> defaultBoardState = [
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
    PLAYER.EMPTY,
  ];

  BoardState({
    @required this.player,
  }) {
    if (player == PLAYER.CIRCLE)
      _enemy = PLAYER.CROSS;
    else
      _enemy = PLAYER.CIRCLE;

    _boardState = [];
    defaultBoardState.forEach((f) {
      _boardState.add(f);
    });
  }

  // -- game over stream --
  PublishSubject<PLAYER> _isGameOverController = PublishSubject<PLAYER>();
  Stream<PLAYER> get isGameOver => _isGameOverController.stream;

  PublishSubject<List<PLAYER>> _boardStateController =
      PublishSubject<List<PLAYER>>();

  Stream<List<PLAYER>> get getBoardState => _boardStateController.stream;

  // helper functions
  void addPlayer(int index) {
    if (_boardState.elementAt(index) == PLAYER.EMPTY) {
      if (_playerTurn) {
        _boardState[index] = player;
      } else {
        _boardState[index] = _enemy;
      }

      _boardStateController.add(_boardState);

      _playerTurn = !_playerTurn;
      var winner = GameLogic.winner(_boardState);

      Future.delayed(Utilities.duration, () {
        if (winner != PLAYER.EMPTY) {
          _isGameOverController.add(winner);
        } else {
          if (!_boardState.contains(PLAYER.EMPTY)) {
            _isGameOverController.add(winner);
          }
        }
      });
    }
  }

  void resetBoard() {
    _boardStateController.add(defaultBoardState);
    _isGameOverController.add(PLAYER.EMPTY);
  }

  dispose() {
    _isGameOverController.close();
  }
}
