import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tic_tac_toe/players/circle/circle.dart';
import 'package:tic_tac_toe/players/cross/cross.dart';
import 'package:tic_tac_toe/players/player.dart';
import 'package:tic_tac_toe/utils.dart';

import 'board_state.dart';

class Board extends StatefulWidget {
  final PLAYER player;
  final Function(PLAYER) onOver;

  Board(this.player, {this.onOver});

  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  BoardState boardState;

  @override
  void initState() {
    boardState = BoardState(player: this.widget.player);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<PLAYER>(
      stream: boardState.isGameOver,
      builder: (context, state) {
        if (state.hasData) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            this.widget.onOver(state.data);
            showDialog(
              context: context,
              barrierDismissible: false,
              child: WillPopScope(
                onWillPop: () {
                  return;
                },
                child: AlertDialog(
                  shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  title: Text('GAME OVER'),
                  content: Row(
                    children: <Widget>[
                      Container(
                        width: 64,
                        height: 64,
                        child: (state.data != PLAYER.EMPTY)
                            ? (state.data == PLAYER.CIRCLE)
                                ? CirclePlayer()
                                : CrossPlayer()
                            : Container(),
                      ),
                      SizedBox(width: 8),
                      Text(
                          (state.data != PLAYER.EMPTY)
                              ? "won this round."
                              : "Its a Tie!",
                          style: TextStyle(fontSize: 18)),
                    ],
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("EXIT"),
                      onPressed: () {
                        SystemNavigator.pop();
                      },
                    ),
                    FlatButton(
                      child: Text("PLAY AGAIN"),
                      onPressed: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                ),
              ),
            );
          });
        }
        return Container(
          constraints: BoxConstraints(
            maxHeight: 320,
            maxWidth: 320,
          ),
          decoration: BoxDecoration(
            color: Color(0XFFf2f4fa),
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 3),
                blurRadius: 8,
                spreadRadius: 2,
                color: Colors.black.withOpacity(0.2),
              ),
            ],
          ),
          margin: const EdgeInsets.all(12.0),
          child: Container(
            margin: EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.black.withOpacity(0.025),
                borderRadius: BorderRadius.circular(9)),
            child: StreamBuilder<List<PLAYER>>(
                stream: boardState.getBoardState,
                initialData: BoardState.defaultBoardState,
                builder: (context, AsyncSnapshot<List<PLAYER>> state) {
                  return GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 3.5,
                      mainAxisSpacing: 3.5,
                    ),
                    itemCount: 9,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: const EdgeInsets.all(12.0),
                        decoration: BoxDecoration(
                          borderRadius: _getBorderRadius(index),
                          color: Color(0XFFf5f7fa),
                        ),
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () {
                            boardState.addPlayer(index);
                          },
                          child: _playerBuilder(state.data[index]),
                        ),
                      );
                    },
                  );
                }),
          ),
        );
      },
    );
  }

  BorderRadius _getBorderRadius(int index) {
    double value = 8;
    switch (index) {
      case 0:
        return BorderRadius.only(topLeft: Radius.circular(value));
      case 2:
        return BorderRadius.only(topRight: Radius.circular(value));
      case 6:
        return BorderRadius.only(bottomLeft: Radius.circular(value));

      case 8:
        return BorderRadius.only(bottomRight: Radius.circular(value));
      default:
        return BorderRadius.all(Radius.circular(0));
    }
  }

  Widget _playerBuilder(PLAYER player) {
    switch (player) {
      case PLAYER.CIRCLE:
        return AnimatedCirclePlayer();
        break;
      case PLAYER.CROSS:
        return AnimatedCrossPlayer();
        break;
      default:
        return Container();
        break;
    }
  }
}
