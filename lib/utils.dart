import 'package:shared_preferences/shared_preferences.dart';

class Utilities {
  static const String _WIN = "win";
  static const String _LOSE = "lose";

  static Future<bool> addWin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int count = await getWin() ?? 0;
    await prefs.setInt(_WIN, count + 1);
    return true;
  }

  static Future<bool> addLose() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int count = await getLose() ?? 0;
    await prefs.setInt(_LOSE, count + 1);
    return true;
  }

  static Future<int> getWin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int count = prefs.getInt(_WIN) ?? 0;
    return count;
  }

  static Future<int> getLose() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int count = prefs.getInt(_LOSE) ?? 0;
    return count;
  }

  static Duration duration = Duration(milliseconds: 600);
}
