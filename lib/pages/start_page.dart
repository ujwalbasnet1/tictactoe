import 'package:flutter/material.dart';
import 'package:tic_tac_toe/pages/pick_your_side_page.dart';
import 'package:tic_tac_toe/players/circle/circle.dart';
import 'package:tic_tac_toe/players/cross/cross.dart';

import '../logo.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  bool showComingSoon = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFe8eaf6),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 48),
          Logo(),
          SizedBox(height: 48),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CrossPlayer(),
              SizedBox(width: 24),
              CirclePlayer(),
            ],
          ),
          SizedBox(height: 48),
          Text(
            'Choose your play mode',
            style: TextStyle(
              color: Color(0XFFAAAAAA),
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 72),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 48),
            width: MediaQuery.of(context).size.width,
            child: RaisedButton(
              child: Text(
                'WITH A FRIEND',
                style: TextStyle(color: Colors.white),
              ),
              color: Colors.blueAccent,
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => PickYourSidePage()));
              },
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
            ),
          ),
          SizedBox(height: 4),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 48),
            width: MediaQuery.of(context).size.width,
            child: OutlineButton(
              child: Text(
                'CONNECT WITH BLUETOOTH',
                style: TextStyle(color: Colors.grey),
              ),
              color: Colors.grey[400],
              onPressed: () => setState(() => showComingSoon = true),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24)),
            ),
          ),
          SizedBox(height: 2),
          (showComingSoon)
              ? Text(
                  'Bluetooth feature coming soon..!!!',
                  style: TextStyle(
                    color: Color(0XFFAAAAAA),
                    fontWeight: FontWeight.normal,
                    fontSize: 13,
                  ),
                )
              : SizedBox.shrink(),
          SizedBox(height: 48),
        ],
      ),
    );
  }
}
