import 'package:flutter/material.dart';
import 'package:tic_tac_toe/players/circle/circle.dart';
import 'package:tic_tac_toe/players/cross/cross.dart';
import 'package:tic_tac_toe/players/player.dart';

import 'game_page.dart';

class PickYourSidePage extends StatefulWidget {
  @override
  _PickYourSidePageState createState() => _PickYourSidePageState();
}

class _PickYourSidePageState extends State<PickYourSidePage> {
  bool isCrossSelected = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFe8eaf6),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                  onTap: () => _tapped(PLAYER.CROSS),
                  child: _Selector(
                    CrossPlayer(),
                    isActive: isCrossSelected,
                  )),
              GestureDetector(
                  onTap: () => _tapped(PLAYER.CIRCLE),
                  child: _Selector(
                    CirclePlayer(),
                    isActive: !isCrossSelected,
                  )),
            ],
          )),
          SizedBox(height: 64),
          RaisedButton(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Text(
                'START',
                style: TextStyle(color: Colors.white),
              ),
            ),
            color: Colors.blueAccent,
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => GamePage(isCrossSelected)));
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
          ),
        ],
      ),
    );
  }

  _tapped(PLAYER player) {
    print('\n\n\Tapped');
    setState(() {
      if (player == PLAYER.CROSS)
        isCrossSelected = true;
      else if (player == PLAYER.CIRCLE) isCrossSelected = false;
    });
  }
}

class _Selector extends StatefulWidget {
  final Player player;
  final bool isActive;

  _Selector(this.player, {this.isActive = false});

  @override
  __SelectorState createState() => __SelectorState();
}

class __SelectorState extends State<_Selector> {
  Color color;

  @override
  void initState() {
    color = (widget.player is CrossPlayer) ? Colors.blue : Colors.orangeAccent;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 120),
      curve: Curves.easeOut,
      opacity: (widget.isActive) ? 1 : 0.45,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          widget.player,
          SizedBox(height: 48),
          Container(
            height: 44,
            width: 44,
            decoration: BoxDecoration(
              border: Border.all(
                color: widget.isActive ? color : Colors.grey,
                width: 7,
              ),
              borderRadius: BorderRadius.circular(48),
            ),
            child: Container(
              height: 32,
              width: 32,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 5),
                borderRadius: BorderRadius.circular(32),
                color: widget.isActive ? color : Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
