import 'package:flutter/material.dart';
import 'package:tic_tac_toe/board/board.dart';
import 'package:tic_tac_toe/players/player.dart';
import 'package:tic_tac_toe/utils.dart';

class GamePage extends StatefulWidget {
  final bool isCrossSelected;

  GamePage(this.isCrossSelected);

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  final TextStyle _baseTextStyle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  final TextStyle _userBaseTextStyle = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w500,
  );

  int win = 0, lose = 0;

  @override
  void initState() {
    init();
    super.initState();
  }

  void init() async {
    var x = await Future.wait(<Future>[
      Utilities.getWin(),
      Utilities.getLose(),
    ]);

    setState(() {
      win = x[0];
      lose = x[1];
    });
  }

  void wonBy(PLAYER player) async {
    if ((this.widget.isCrossSelected && player == PLAYER.CROSS) ||
        (!this.widget.isCrossSelected && player == PLAYER.CIRCLE)) {
      await Utilities.addWin();
    } else if (player == PLAYER.EMPTY) {
      print("DON'T SAVE: TIE");
    } else {
      await Utilities.addLose();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFe8eaf6),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 32),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          flex: 4,
                          child: Text(
                            'YOU',
                            style: _userBaseTextStyle,
                            textAlign: TextAlign.end,
                          )),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        padding:
                            EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(32),
                          color: Colors.grey[50],
                          boxShadow: [
                            BoxShadow(
                              offset: Offset(0, 4),
                              blurRadius: 8,
                              spreadRadius: 4,
                              color: Colors.black.withOpacity(0.1),
                            ),
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('$win', style: _baseTextStyle),
                            Text(' - ', style: _baseTextStyle),
                            Text('$lose', style: _baseTextStyle),
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 4,
                          child: Text(
                            'FRIEND',
                            style: _userBaseTextStyle,
                            textAlign: TextAlign.start,
                          )),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                Board(
                    (this.widget.isCrossSelected)
                        ? PLAYER.CROSS
                        : PLAYER.CIRCLE,
                    onOver: wonBy),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
