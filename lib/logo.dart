import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  final TextStyle _style = TextStyle(
    color: Color(0XFFAAAAAA),
    fontWeight: FontWeight.bold,
    fontSize: 48,
  );
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Text(
          'T',
          style: _style.copyWith(fontSize: 120, height: 0, color: Colors.blue),
        ),
        SizedBox(width: 4),
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              'ic',
              style: _style.copyWith(
                fontSize: 20,
                color: Colors.blue,
                letterSpacing: 12,
              ),
            ),
            Text(
              'ac',
              style: _style.copyWith(
                fontSize: 20,
                letterSpacing: 12,
              ),
            ),
            Text(
              'oe',
              style: _style.copyWith(
                fontSize: 20,
                color: Colors.orangeAccent,
                letterSpacing: 12,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
