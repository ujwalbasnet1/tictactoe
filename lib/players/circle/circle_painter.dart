import 'package:flutter/material.dart';
import 'dart:math';

class CirclePainter extends CustomPainter {
  final double fraction;

  CirclePainter({this.fraction = 1});

  @override
  void paint(Canvas canvas, Size size) {
    // circle draw

    Paint paint = Paint();
    paint.color = Colors.orangeAccent;
    paint.strokeWidth = 22;
    paint.strokeCap = StrokeCap.round;
    paint.style = PaintingStyle.stroke;

    Offset center = Offset(size.width * 0.5, size.height * 0.45);
    double diameter = (size.width < size.height) ? size.width : size.height;
    diameter = diameter - 20;

    canvas.drawArc(
      Rect.fromLTRB(10, 10, size.width - 10, size.height - 10),
      -pi / 2,
      pi * 2 * fraction,
      false,
      paint,
    );

//    canvas.drawCircle(center, diameter * 0.45, paint);

    // shadow draw

    Offset startP = Offset(0, size.height * 0.75);
    Offset ovalSize = Offset(size.width, size.height * 0.25);

    Rect rect = Rect.fromLTWH(startP.dx, startP.dy, ovalSize.dx, ovalSize.dy);

    Path path = Path()..addOval(rect);
    canvas.drawShadow(path, Colors.orangeAccent.withOpacity(0.5), 6, true);
  }

  @override
  bool shouldRepaint(CirclePainter oldDelegate) =>
      (fraction != oldDelegate.fraction);
}
