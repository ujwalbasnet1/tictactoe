import 'package:flutter/material.dart';
import 'package:tic_tac_toe/utils.dart';

import '../player.dart';
import 'circle_painter.dart';

class CirclePlayer extends Player {
  @override
  CustomPainter customPainter() => CirclePainter();

  @override
  PLAYER player() => PLAYER.CIRCLE;
}

class AnimatedCirclePlayer extends StatefulWidget {
  @override
  _AnimatedCirclePlayerState createState() => _AnimatedCirclePlayerState();
}

class _AnimatedCirclePlayerState extends State<AnimatedCirclePlayer>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      duration: Utilities.duration,
      vsync: this,
    );

    final Animation curve = CurvedAnimation(
      parent: controller,
      curve: Curves.easeIn,
    );

    animation = Tween<double>(begin: 0, end: 1).animate(curve)
      ..addListener(() {
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 72,
      width: 72,
      child: CustomPaint(
        painter: CirclePainter(
          fraction: animation.value,
        ),
      ),
    );
  }
}
