import 'package:flutter/material.dart';

class CrossPainter extends CustomPainter {
  final double fraction;

  CrossPainter({this.fraction = 1});

  @override
  void paint(Canvas canvas, Size size) {
    // cross draw

    Paint paint = Paint();
    paint.color = Colors.blue;
    paint.strokeWidth = 25;
    paint.strokeCap = StrokeCap.round;

    double firstFraction = 0;
    double secondFraction = 0;

    if (fraction < 0.5) {
      firstFraction = fraction / 0.5;
      secondFraction = 0;
    } else {
      firstFraction = 1;
      secondFraction = (fraction - 0.5) / 0.5;
    }

    Offset p1 = Offset(10, 10);
    Offset p2 = Offset(
      (size.width - 10) * firstFraction + 10 * (1 - firstFraction),
      (size.height * 0.95 - 10) * firstFraction + 10 * (1 - firstFraction),
    );

    Offset _p1 = Offset(size.width - 10, 10);
    Offset _p2 = Offset(
      (size.width - 10) * (1 - secondFraction) + 10 * secondFraction,
      (size.height * 0.95 - 10) * secondFraction + 10 * (1 - secondFraction),
    );

    canvas.drawLine(p1, p2, paint);

    if (fraction >= 0.5) {
      canvas.drawLine(_p1, _p2, paint);
    }

    // shadow draw
    Offset startP = Offset(0, size.height * 0.75);
    Offset ovalSize = Offset(size.width, size.height * 0.25);

    Rect rect = Rect.fromLTWH(startP.dx, startP.dy, ovalSize.dx, ovalSize.dy);

    Path path = Path()..addOval(rect);
    canvas.drawShadow(path, Colors.blue.withOpacity(0.5), 6, true);
  }

  @override
  bool shouldRepaint(CrossPainter oldDelegate) =>
      fraction != oldDelegate.fraction;
}
