import 'package:flutter/material.dart';
import 'package:tic_tac_toe/utils.dart';

import '../player.dart';
import 'cross_painter.dart';

class CrossPlayer extends Player {
  @override
  CustomPainter customPainter() => CrossPainter();

  @override
  PLAYER player() => PLAYER.CROSS;
}

class AnimatedCrossPlayer extends StatefulWidget {
  @override
  _AnimatedCrossPlayerState createState() => _AnimatedCrossPlayerState();
}

class _AnimatedCrossPlayerState extends State<AnimatedCrossPlayer>
    with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      duration: Utilities.duration,
      vsync: this,
    );

    final Animation curve = CurvedAnimation(
      parent: controller,
      curve: Curves.easeIn,
    );

    animation = Tween<double>(begin: 0, end: 1).animate(curve)
      ..addListener(() {
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 72,
      width: 72,
      child: CustomPaint(
        painter: CrossPainter(
          fraction: animation.value,
        ),
      ),
    );
  }
}
