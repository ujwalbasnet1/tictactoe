import 'package:flutter/material.dart';

enum PLAYER {
  CROSS,
  CIRCLE,
  EMPTY,
}

abstract class Player extends StatelessWidget {
  CustomPainter customPainter();

  PLAYER player();

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 72,
      width: 72,
      child: CustomPaint(
        painter: customPainter(),
      ),
    );
  }
}
